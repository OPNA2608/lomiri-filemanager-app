# Disable TagLib in folderlistmodel plugin
ADD_DEFINITIONS(-DDO_NOT_USE_TAG_LIB)

add_subdirectory(folderlistmodel)
add_subdirectory(placesmodel)
add_subdirectory(archives)
